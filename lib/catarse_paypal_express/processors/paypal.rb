module CatarsePaypalExpress
  module Processors
    class Paypal

      def process!(backer, data)
        status = data["failure_code"]

        notification = backer.payment_notifications.new({
          extra_data: data
        })

        notification.save!

        backer.confirm! if success_payment?(status)
      rescue Exception => e
        ::Airbrake.notify({ :error_class => "Stripe Processor Error", :error_message => "Stripe Processor Error: #{e.inspect}", :parameters => data}) rescue nil
      end

      protected

      def success_payment?(status)
        status.blank?
      end

    end
  end
end
