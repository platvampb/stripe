CATARSE.PayPalForm = CATARSE.UserDocument.extend({
  el: '#catarse_paypal_express_form',

  events: {
    'submit form#new_card_form': 'onSubmitToStripe',
    'change #new_card_form input[name="credit_card_old_new"]' : 'onChangeOldNewCC',
  },

  initialize: function() {
    this.loader = $('.loader')
  },

  onChangeOldNewCC: function(e) {
    if ($('input:radio[name=credit_card_old_new]:checked').val() == "old") {
      $('li.card_number input, li.cvc input, li.exp select').prop('disabled', true)
      $('li.card_number label, li.cvc label, li.exp label').addClass('grey')
      $('li.last_card label').removeClass('grey')
    } else {
      $('li.card_number input, li.cvc input, li.exp select').prop('disabled', false)
      $('li.card_number label, li.cvc label, li.exp label').removeClass('grey')
      $('li.last_card label').addClass('grey')
    }
  },

  onSubmitToStripe: function(e) {
    if ($('input:radio[name=credit_card_old_new]:checked').val() == "new") {
      e.preventDefault()
      Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
      Stripe.createToken($(e.target), this.handleStripeResponse)
      return false
    }
  },

  handleStripeResponse: function(status, response) {
    var $form = $('#new_card_form')

    if (response.error) {
      $('#stripe_error').text(response.error.message)
      $('input[type=submit]').attr('disabled', false) 
    } else {
      var token = response.id
      $form.append($('<input type="hidden" name="stripeToken" />').val(token))
      $form.get(0).submit()
    }
  }
});
