require 'catarse_paypal_express/processors'

module CatarsePaypalExpress::Payment
  class PaypalExpressController < ApplicationController
    skip_before_filter :verify_authenticity_token, :only => [:notifications]
    skip_before_filter :detect_locale, :only => [:notifications]
    skip_before_filter :set_locale, :only => [:notifications]
    skip_before_filter :force_http

    before_filter :setup_gateway

    SCOPE = "projects.backers.checkout"

    layout :false

    def review
      @public_key = ::Configuration[:stripe_public_key]
      last_backer = current_user.backs.confirmed.find(:first, conditions: ["payment_method=?", "Stripe"], order: "confirmed_at DESC")
      if last_backer && last_backer.payment_response
        @last_payment_id = last_backer.id
        @last_cc = JSON.parse(last_backer.payment_response)['card']
      end
    end

    def ipn
      backer = Backer.where(:payment_id => params['txn_id']).first
      if backer
        notification = backer.payment_notifications.new({
          extra_data: JSON.parse(params.to_json.force_encoding(params['charset']).encode('utf-8'))
        })
        notification.save!
        backer.update_attributes({
          :payment_service_fee => params['mc_fee'],
          :payer_email => params['payer_email']
        })
      end
      return render status: 200, nothing: true
    rescue Exception => e
      ::Airbrake.notify({ :error_class => "Stripe Notification Error", :error_message => "Stripe Notification Error: #{e.inspect}", :parameters => params}) rescue nil
      return render status: 200, nothing: true
    end

    def notifications
      backer = Backer.find params[:id]
      response = @@gateway.details_for(backer.payment_token)
      if response.params['transaction_id'] == params['txn_id']
        build_notification(backer, response.params)
        render status: 200, nothing: true
      else
        render status: 404, nothing: true
      end
    rescue Exception => e
      ::Airbrake.notify({ :error_class => "Stripe Notification Error", :error_message => "Stripe Notification Error: #{e.inspect}", :parameters => params}) rescue nil
      render status: 404, nothing: true
    end

    def pay
      backer = current_user.backs.find params[:id]
      begin
        customer_id = ''
        if params[:last_payment_id].present? && params[:credit_card_old_new] == 'old'
          last_backer = current_user.backs.find params[:last_payment_id]
          customer_id = JSON.parse(last_backer.payment_response)['customer']
        else
          response = @@gateway.store(params[:stripeToken], {set_default: true})
          customer_id = response.params['id']
        end

        response = @@gateway.purchase(backer.price_in_cents, nil, {
          currency: 'cad',
          customer: customer_id,
          description: t('stripe_description', scope: SCOPE, :project_name => backer.project.name, :value => backer.display_value)
        })

        backer.update_attribute :payment_method, 'Stripe'
        backer.update_attribute :payment_token, response.params['id']
        backer.update_attribute :payment_response, response.params.to_json
        session[:_payment_token] = response.params['id']

        build_notification(backer, response.params)

        paypal_flash_success
        redirect_to main_app.thank_you_path
        #redirect_to main_app.(project_id: backer.project.id, id: backer.id)

      rescue Exception => e
        ::Airbrake.notify({ :error_class => "Stripe Error", :error_message => "Stripe Error: #{e.inspect}", :parameters => params}) rescue nil
        Rails.logger.info "-----> #{e.inspect}"
        paypal_flash_error
        return redirect_to main_app.new_project_backer_path(backer.project)
      end
    end

    def cancel
      backer = current_user.backs.find params[:id]
      flash[:failure] = t('stripe_cancel', scope: SCOPE)
      redirect_to main_app.new_project_backer_path(backer.project)
    end

  private

    def build_notification(backer, data)
      processor = CatarsePaypalExpress::Processors::Paypal.new
      processor.process!(backer, data)
    end

    def paypal_flash_error
      flash[:failure] = t('paypal_error', scope: SCOPE)
    end

    def paypal_flash_success
      flash[:success] = t('success', scope: SCOPE)
    end

    def setup_gateway
      if ::Configuration[:stripe_api_key]
        @@gateway ||= ActiveMerchant::Billing::StripeGateway.new({
          :login => ::Configuration[:stripe_api_key]
        })
      else
        puts "[Stripe] An API Certificate or API Signature is required to make requests to Stripe"
      end
    end
  end
end
